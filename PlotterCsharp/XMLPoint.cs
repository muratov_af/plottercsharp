﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace PlotterCsharp
{
    [XmlRoot("PointList")]
    public class XMLPoint
    {
        [XmlElement("point")]
        public List<Step> Steps { get; set; }

        public void print()
        {
            foreach (Step s in Steps)
            {
                Console.WriteLine("x = " + s.x + " ; y = " + s.y);
            }
        }

        public float[] getXarr()
        {
            float[] ret = new float[Steps.Count];
            for (int i = 0; i < Steps.Count; i++)
            {
                ret[i] = Convert.ToSingle(Steps[i].x);
            }
            return ret;
        }
        public float[] getYarr()
        {
            float[] ret = new float[Steps.Count];
            for (int i = 0; i < Steps.Count; i++)
            {
                ret[i] = Convert.ToSingle(Steps[i].y);
            }
            return ret;
        }
    }
}
public class Step
{
    [XmlElement("x")]
    public string x { get; set; }
    [XmlElement("y")]
    public string y { get; set; }
}