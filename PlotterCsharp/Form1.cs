﻿using System;
using System.IO;
using System.Windows.Forms;
using System.Xml.Serialization;

namespace PlotterCsharp
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(XMLPoint));
            XMLPoint result;
            using (FileStream fileStream = new FileStream("data.xml", FileMode.Open))
            {
                result = (XMLPoint)serializer.Deserialize(fileStream);
                result.print();
            }
            chart1.Series["Series1"].Points.DataBindXY(result.getXarr(), result.getYarr());

        }
    }
}
